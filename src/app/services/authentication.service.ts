import { firebase } from 'firebaseui-angular';
import { Injectable } from '@angular/core';
import { User } from 'firebase/auth';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public loginStatusChanged = new Subject<User | null>();
  public currentUser: User | null = null;

  constructor() {
    firebase.auth().onAuthStateChanged((user) => {
      this.currentUser = user as User | null;
      this.loginStatusChanged.next(this.currentUser);
    });
  }

  public isUserSignedIn(): boolean {
    return firebase.auth().currentUser !== null;
  }

  public logout(): void {
    firebase.auth().signOut();
  }
}
