import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulauswahlComponent } from './modulauswahl.component';
import { ButtonModule } from 'primeng/button';

describe('ModulauswahlComponent', () => {
  let component: ModulauswahlComponent;
  let fixture: ComponentFixture<ModulauswahlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModulauswahlComponent],
      imports: [RouterTestingModule, ButtonModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulauswahlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
