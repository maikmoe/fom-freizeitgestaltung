import { Subscription } from 'rxjs';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { Component, OnDestroy } from '@angular/core';
import { MenuItem, PrimeNGConfig } from 'primeng/api';
import { User } from 'firebase/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnDestroy {
  public items: MenuItem[] = [
    {
      label: 'Quellcode',
      icon: 'pi pi-github',
      url: 'https://gitlab.com/maikmoe/fom-freizeitgestaltung',
    },
    {
      label: 'Modulauswahl',
      icon: 'pi pi-th-large',
      routerLink: 'modulauswahl',
      disabled: true,
    },
  ];

  public currentUser: User | null = null;

  private loginStatusChangedSub: Subscription;

  constructor(
    primengConfig: PrimeNGConfig,
    private router: Router,
    private authService: AuthenticationService
  ) {
    primengConfig.ripple = true;
    this.loginStatusChangedSub = this.authService.loginStatusChanged.subscribe(
      (user) => {
        this.currentUser = user;
        this.items[1].disabled = !this.currentUser;
        if (!this.currentUser && this.router.url === '/modulauswahl') {
          this.router.navigate(['/']);
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.loginStatusChangedSub.unsubscribe();
  }

  public navigateToLogin(): void {
    this.router.navigate(['/login']);
  }

  public navigateToMainpage(): void {
    this.router.navigate(['']);
  }

  public logoutUser(): void {
    this.authService.logout();
  }
}
