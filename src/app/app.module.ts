import { AngularFirestore } from '@angular/fire/compat/firestore';
import { firebaseUiAuthConfig } from './firebaseUI.config';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { LoginComponent } from './pages/login/login.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { ModulauswahlComponent } from './pages/modulauswahl/modulauswahl.component';
import { FirebaseUIModule } from 'firebaseui-angular';
import { DividerModule } from 'primeng/divider';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from 'primeng/api';
import { AngularFireModule } from '@angular/fire/compat';
import { TosPrivacyComponent } from './pages/tos-privacy/tos-privacy.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { firebase } from 'firebaseui-angular';
import { MapsComponent } from './pages/module/maps/maps.component';
import { ChatComponent } from './pages/module/chat/chat.component';
import { GMapModule } from 'primeng/gmap';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WelcomeComponent,
    ModulauswahlComponent,
    TosPrivacyComponent,
    MapsComponent,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ButtonModule,
    SharedModule,
    DividerModule,
    MenubarModule,
    GMapModule,
    TooltipModule,
    InputTextModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    provideAuth(() => getAuth()),
    AngularFireModule.initializeApp(environment.firebase),
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {
    firebase.initializeApp(environment.firebase);
  }
}
