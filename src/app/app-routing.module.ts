import { MapsComponent } from './pages/module/maps/maps.component';
import { ChatComponent } from './pages/module/chat/chat.component';
import { TosPrivacyComponent } from './pages/tos-privacy/tos-privacy.component';
import { ModulauswahlComponent } from './pages/modulauswahl/modulauswahl.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { LoginComponent } from './pages/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const moduleRoutes: Routes = [
  {
    path: 'chat',
    component: ChatComponent,
  },
  {
    path: 'maps',
    component: MapsComponent,
  },
];

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'modulauswahl',
    component: ModulauswahlComponent,
  },
  {
    path: 'welcome',
    component: WelcomeComponent,
  },
  {
    path: 'tos-privacy',
    component: TosPrivacyComponent,
  },
  ...moduleRoutes,
  {
    path: '**',
    component: WelcomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
