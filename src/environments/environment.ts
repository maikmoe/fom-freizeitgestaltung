// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'fom-freizeitgestalltung',
    appId: '1:162606408591:web:95b496abee51da6c19a1fa',
    storageBucket: 'fom-freizeitgestalltung.appspot.com',
    apiKey: 'AIzaSyAvCs1UHR6NxTJHV2A5Nk8i4dwiaTTzKCg',
    authDomain: 'fom-freizeitgestalltung.firebaseapp.com',
    messagingSenderId: '162606408591',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
